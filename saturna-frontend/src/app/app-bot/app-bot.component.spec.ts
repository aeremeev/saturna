import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppBotComponent } from './app-bot.component';

describe('AppBotComponent', () => {
  let component: AppBotComponent;
  let fixture: ComponentFixture<AppBotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppBotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppBotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
