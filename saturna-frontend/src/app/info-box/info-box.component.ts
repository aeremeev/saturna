import { Component, OnInit } from '@angular/core';
import { ProductService } from '../Products/Services/product.service';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-info-box',
  templateUrl: './info-box.component.html',
  styleUrls: ['./info-box.component.scss']
})

export class InfoBoxComponent implements OnInit {

  constructor(private productService: ProductService, private route: ActivatedRoute) {
  }

  private productId;

  ngOnInit() {
  }

  get productsList() {
    return this.productService.getProducts();
  }
  get activeProduct() {
    const productId = this.route.params.subscribe(params => {
      this.productId = params.id;
    });

    return this.productService.getProductById(this.productId) || { 'name': 'product not found' };
  }
}
