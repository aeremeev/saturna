import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
@Injectable({
    providedIn: 'root'
})
export class LocationService {
    private locations;

    constructor(private http: Http) {
    }

    public getLocations() {
        if (this.locations) {
            return this.locations;
        }

        this.http.get('assets/locations.json').subscribe(async res =>
            this.locations = await res.json());
    }
    public getLocationsById(id: string) {
        if (this.locations && this.locations.adresses) {
            return this.locations.adresses.filter(f => f.id === id)[0];
        }

        this.http.get('assets/locations.json').subscribe(async res =>
            this.locations = await res.json());
    }
}
