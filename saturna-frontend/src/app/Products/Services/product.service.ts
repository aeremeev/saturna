import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
    providedIn: 'root'
})
export class ProductService {

    private products;

    constructor(private http: Http) {
    }

    public getProducts() {
        if (this.products) {
            return this.products;
        }

        this.http.get('assets/products.json').subscribe(async res =>
            this.products = await res.json());
    }
    public getProductById(id: string) {
        if (this.products && this.products.products) {
            return this.products.products.filter(f => f.id === id)[0];
        }

        this.http.get('assets/products.json').subscribe(async res =>
            this.products = await res.json());
    }

    public getOffersForProductId(productId: string) {
        if (this.products && this.products.offers) {
            return this.products.offers.filter(f => f.item === productId);
        }

        this.http.get('assets/products.json').subscribe(async res =>
            this.products = await res.json());
    }
}
