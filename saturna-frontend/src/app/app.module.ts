import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { LocationViewComponent } from 'src/app/location-view-component/location-view-component';
import { AppBotComponent } from './app-bot/app-bot.component';
import { AppComponent } from './app.component';
import { InfoBoxComponent } from './info-box/info-box.component';
import { OfferBoxComponent } from './offer-box/offer-box.component';


const appRoutes: Routes = [
  { path: 'Produktinfos/:id', component: InfoBoxComponent },
  { path: 'Angebot/:itemId', component: OfferBoxComponent},
  { path: 'Standort/:id', component: LocationViewComponent },
  { path: '', component: AppBotComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AppBotComponent,
    InfoBoxComponent,
    OfferBoxComponent,
    LocationViewComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes
      )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
