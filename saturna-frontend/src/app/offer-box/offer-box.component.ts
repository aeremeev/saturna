import { Component, OnInit } from '@angular/core';
import { ProductService } from '../Products/Services/product.service';

import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-offer-box',
  templateUrl: './offer-box.component.html',
  styleUrls: ['./offer-box.component.css']
})
export class OfferBoxComponent implements OnInit {

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  private productId;
  private products = {
      "none": {
          "id": "none",
          "name": "none"
      }
  };

  ngOnInit() {
  }

  get activeOffers() {
    const productId = this.route.params.subscribe(params => {
      this.productId = params.itemId;
    });

    this.products[this.productId] = this.productService.getProductById(this.productId);

    return this.productService.getOffersForProductId(this.productId)
    || [{
        "id": "none",
        "item": "none",
        "price": "none",
        "description":"no offers for product found"
    }];
    
  }

}
