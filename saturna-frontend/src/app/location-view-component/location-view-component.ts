import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {} from '@types/googlemaps';
import { LocationService } from 'src/app/Locations/Services/location.service';
@Component({
  selector: 'app-location-view',
  templateUrl: './location-view-component.html',
  styleUrls: ['./location-view-component.scss']
})

export class LocationViewComponent implements OnInit {

  constructor(private locationServices: LocationService, private route: ActivatedRoute) {
  }

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  private locationid: string;
  private locationName: string;
  ngOnInit() {
    this.map = new google.maps.Map(this.gmapElement.nativeElement, {
      center: new google.maps.LatLng(48.181319, 11.534125),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
  }
  get getLoctions() {
    return this.locationServices.getLocations();
  }
  get activeLocations() {
    const locationid = this.route.params.subscribe(params => {
      this.locationid = params.id;
    });
    return this.locationServices.getLocationsById(this.locationid) || { 'name': 'location not found' };
  }
}
